import { Component, Element, State, Event, EventEmitter } from '@stencil/core';
import accessibleAutocomplete from 'accessible-autocomplete';
import Pikaday from 'pikaday';

@Component({
  tag: 'tn-from-to',
  styleUrl: 'tn-from-to.css',
  shadow: false
})
export class TnFromTo {

  private stationsFrom: any[] = [];
  private stationsTo: any[] = [];

  private evaFrom: string;
  private evaTo: string;
  private date: string;

  @Element() private element: HTMLElement;

  @State() private fromRendered: boolean = false;
  @State() private showTo: boolean = false;

  @Event() fromToSubmitted: EventEmitter;

  componentDidLoad() {
    this.initPikaday();

    let xhr = new XMLHttpRequest();
    xhr.open('GET', `http://nachtzug.herokuapp.com/api/v1/trainite/stationlist`, true);
    xhr.onreadystatechange = () => {
        if (xhr.readyState === XMLHttpRequest.DONE && xhr.status === 200) {
            this.stationsFrom = JSON.parse(xhr.response)['message'];
            this.initFrom();
        }
    }    
    xhr.send(null);
  }

  initPikaday() {
    new Pikaday({
      field: document.querySelector('#from-to-date'),
      format: 'DD.MM.YYYY',
      onSelect: (date) => {
        this.date = date;
      }
    });
  }

  initFrom() {
    if (this.fromRendered) return;
    this.fromRendered = true;

    let sourceFrom = [];
    this.stationsFrom.forEach((station: any) => {
      sourceFrom.push(station.name);
    });

    accessibleAutocomplete({
      element: this.element.querySelector('#from-to-from-container'),
      id: 'from-to-from',
      source: sourceFrom,
      onConfirm: (stationName: any) => {
        this.stationsFrom.forEach((station: any) => {
          if (station.name === stationName) {
            this.evaFrom = station.eva;
            this.initTo();
            return;
          }
        });
      }
    });
  }

  initTo() {
    let xhr = new XMLHttpRequest();

    xhr.open('GET', `http://nachtzug.herokuapp.com/api/v1/trainite/destinations?bhfDep=${this.evaFrom}`, true);
    xhr.onreadystatechange = () => {
        if (xhr.readyState === XMLHttpRequest.DONE && xhr.status === 200) {
            this.stationsTo = JSON.parse(xhr.response)['message'];
            let sourceTo = [];
            this.stationsTo.forEach((station: any) => {
              sourceTo.push(station.name);
            });
            this.showTo = true;
            this.element.querySelector('#from-to-to-container').innerHTML = '';

            accessibleAutocomplete({
              element: this.element.querySelector('#from-to-to-container'),
              id: 'from-to-to',
              source: sourceTo,
              onConfirm: (stationName: any) => {
                this.stationsTo.forEach((station: any) => {
                  if (station.name === stationName) {
                    this.evaTo = station.eva;
                    return;
                  }
                });
              }
            });
        }
    }    
    xhr.send(null);
  }

  handleSubmit(e: Event) {
    e.preventDefault();

    this.fromToSubmitted.emit({
      evaFrom: this.evaFrom,
      evaTo: this.evaTo,
      depDate: this.date
    });
  }

  render() {      
    return (
      <div class="from-to">
        <h1>Ausgeschlafen durch Europa!</h1>

        <p class="leadin">Abends gemütlich in den Zug steigen, nachts angenehm schlafen, morgens entspannt und nachhaltig direkt am Ziel aufwachen. Verlockend, oder?</p>

        <form onSubmit={(e: Event) => this.handleSubmit(e)}>
          <div class="input-group">
            <label htmlFor="from-to-from">Abfahrtsort</label>
            <div id="from-to-from-container"></div>
          </div>

          <div class="input-group">
            <label htmlFor="from-to-to">Zielort</label>
            { this.showTo 
              ?
                <div>
                  <div id="from-to-to-container" style={{'display': 'block'}}></div>
                  <input type="text" id="from-to-to" disabled={true} style={{'display': 'none'}}/>
                </div>
              :
                <div>
                  <div id="from-to-to-container" style={{'display': 'none'}}></div>
                  <input type="text" id="from-to-to" disabled={true} style={{'display': 'block'}}/>
                </div>
            }
          </div>

          <div class="input-group">
            <label htmlFor="from-to-date">Datum</label>
            <input type="text" id="from-to-date" class="from-to-date-picker is-below" placeholder="TT.MM.JJJJ" />
          </div>

          <div class="input-group">
            <input type="submit" value="Suchen" />
          </div>
        </form>
      </div>
    );
  }
}
