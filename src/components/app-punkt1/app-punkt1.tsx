import { Component, Prop } from '@stencil/core';
import { MatchResults } from '@stencil/router';

@Component({
  tag: 'app-punkt1',
  styleUrl: 'app-punkt1.css',
  shadow: false
})
export class AppPunkt1 {
  @Prop() match: MatchResults;

  normalize(name: string): string {
    if (name) {
      return name.substr(0, 1).toUpperCase() + name.substr(1).toLowerCase();
    }
    return '';
  }

  render() {
 
      return (
        <div class="app-punkt1">
          <h1>Punkt 1</h1>
          <p></p>
        </div>
      );     
  }
}
