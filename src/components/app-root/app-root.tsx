import { Component } from '@stencil/core';


@Component({
  tag: 'app-root',
  styleUrl: 'app-root.css',
  shadow: false
})
export class AppRoot {

  render() {
    return (
      <div class="uk-offcanvas-content">
        <div style={{ position: 'relative', minHeight: '100%' }}>
          <div class="uk-navbar-container uk-navbar-transparent">
            <div class="uk-container">
              <div class="hr-shadow"></div>
              <nav class="uk-navbar">
                <div class="uk-navbar-left">
                {/*   <a uk-navbar-toggle-icon="" href="#offcanvas-overlay-reveal" uk-toggle
                    class="uk-navbar-toggle uk-hidden@m uk-navbar-toggle-icon uk-icon"></a> */}
                     <stencil-route-link url='/'>
                    <img style={{ maxWidth: '180px', padding:'20px' }} src="/assets/images/logo_trainite.svg" alt="" />
                  </stencil-route-link>
                  {/* <ul class="uk-navbar-nav uk-visible@m">
                    <li class="uk-active">
                      <stencil-route-link url='/'>Home</stencil-route-link></li>

                    <li><stencil-route-link url='/punkt1'>Punkt1</stencil-route-link></li>

                    <li><stencil-route-link url='/profile/Punkt2'>Punkt2</stencil-route-link></li>

                    <li><stencil-route-link url='/profile/Punkt3'>Punkt3</stencil-route-link></li>

                  </ul> */}

                </div>
              {/*   <div class="uk-navbar-right">
                  <stencil-route-link url='/'>
                    <img style={{ maxWidth: '80px' }} src="/assets/images/logo_stars@2x.png" alt="" />
                  </stencil-route-link>
                </div> */}
              </nav>

            </div>
          </div>

          <main>
            <stencil-router>
              <stencil-route-switch scrollTopOffset={0}>
                <stencil-route url='/' component='app-home' exact={true} />
                <stencil-route url='/punkt1' component='app-punkt1' />
                <stencil-route url='/destination/:name' component='app-destination' />
              </stencil-route-switch>
            </stencil-router>
          </main>
        </div>
        <div style={{ background: '#084358',paddingBottom: '20px' }}>
          <div class="uk-container site-footer" style={{ textAlign: 'center', paddingTop: '14px', paddingBottom: '14px;', color: 'white' }}>
            <a href="mailto:info@trainite.eu">Kontakt</a><br />
            Made with &#10084; Drei Länder Hack
          </div>
        </div>
        <div id="offcanvas-overlay-reveal" uk-offcanvas="mode: reveal; overlay: true">
          <div class="uk-offcanvas-bar">
            <button class="uk-offcanvas-close" type="button" uk-close></button>

            <ul class="uk-nav uk-nav-default">

              <li class="uk-active"><stencil-route-link url='/'>Home</stencil-route-link></li>

              <li><stencil-route-link url='/profile/Punkt1'>Punkt1</stencil-route-link></li>

              <li><stencil-route-link url='/profile/Punkt2'>Punkt2</stencil-route-link></li>

              <li><stencil-route-link url='/profile/Punkt3'>Punkt3</stencil-route-link></li>


            </ul>

          </div>
        </div>

      </div>


    );
  }
}
