import { Component, Method, State } from '@stencil/core';

@Component({
  tag: 'tn-from-to-result',
  styleUrl: 'tn-from-to-result.css',
  shadow: false
})
export class TnFromToResult {

    private fromTo: any;

    @State() data: any[] = [];

    @Method()
    refresh(fromTo: any) {
        this.fromTo = fromTo;

        const xhr = new XMLHttpRequest();
        xhr.open('GET', `http://nachtzug.herokuapp.com/api/v1/trainite/timetable?bhfDep=${fromTo.evaFrom}&bhfArr=${fromTo.evaTo}`, true);
        xhr.onreadystatechange = () => {
            if (xhr.readyState === XMLHttpRequest.DONE && xhr.status === 200) {
                this.data = JSON.parse(xhr.response)['message'];
            }
        }    
        xhr.send(null);
    }

    handleConnectionAction(connection: any) {
        const day = ('0' + this.fromTo.depDate.getDate()).slice(-2); 
        const month = ('0' + (this.fromTo.depDate.getMonth() + 1)).slice(-2);
        const year = this.fromTo.depDate.getFullYear();

        const dateString = year + '-' + month + '-' + day;

        window.open(
            `https://tickets.oebb.at/de/ticket?outwardDateTime=${dateString}T${connection.time_dep}&stationOrigEva=${this.fromTo.evaFrom}&stationDestEva=${this.fromTo.evaTo}`,
            '_blank'
        );
    }

    render() {
        if (!this.data.length) return;

        return (
            <div>
                <h2>
                    { this.data.length && this.data.length > 1 ? <span>{this.data.length} Verbindungen</span> : <span>{this.data.length} Verbindung</span> }
                </h2>

                {this.data.map((connection: any) =>
                    <div class="connection">
                        <div class="connection__train-number">
                            {connection.trainNumber} ({connection.operator})
                        </div>
                        <div class="connection__train-details">
                            <div class="connection__train-from">
                                Ab <span class="connection__dep">{connection.name_dep}</span>
                                {connection.time_dep}
                            </div>

                            <div class="connection__train-duration">
                                Dauer {connection.duration}
                            </div>

                            <div class="connection__train-to">
                                An <span class="connection__arr">{connection.name_arr}</span>
                                {connection.time_arr}
                            </div>

                            <div class="connection__action">
                                <button onClick={() => this.handleConnectionAction(connection)}>&rang;</button>
                                <span class="connection__price">ab €&nbsp;{connection.priceFrom},-</span>
                            </div>
                        </div>
                    </div>
                )}
            </div>
        )
    }


}
