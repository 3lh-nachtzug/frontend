import { Component, Prop } from '@stencil/core';
import { MatchResults } from '@stencil/router';

@Component({
  tag: 'app-destination',
  styleUrl: 'app-destination.css',
  shadow: false
})
export class AppDestination {
  @Prop() match: MatchResults;

  normalize(name: string): string {
    if (name) {
      return name.substr(0, 1).toUpperCase() + name.substr(1).toLowerCase();
    }
    return '';
  }

  componentWillLoad(){
    console.log("componentWillLoad")
    document.body.classList.add("hamburg")
    window.scrollTo(0,0);
  }
  renderPage() {
    if (this.match && this.match.params.name) {
      return (
        <div class="app-destination">
         {/*  <p>
          {this.normalize(this.match.params.name)}
      
          </p> */}
          <p style={{textAlign:'center', fontSize : '20px',marginTop: '30px',marginBottom: '40px'}}><strong>Es gibt viel zu sehen und viel zu hören im hohen Norden: die Signale der Schiffe im nächtlichen Nebel, das Möwengeschrei, Seemannsgarn. Denn vom Hamburger Hafen geht es ab in die große, weite Welt – aber warum eigentlich? Die junge Metropole hat so vieles zu bieten: Kunst, Architektur, Szeneviertel, Hafenluft.</strong></p>
          <div class="uk-child-width-expand@s uk-text-center" uk-grid>
                            <div class="icon-grid">
                                <img src="/assets/images/hafen.jpg" alt="Hafen" /></div>

                            <div class="icon-grid"><img src="/assets/images/elbphilharmonie.jpg" alt="Nachhaltig" /> </div>

                            <div class="icon-grid"><img src="/assets/images/hafen2.jpg" alt="Nacht" /></div>
                        </div>
          <p>Mit dem Nightjet erreicht man von Wien, Innsbruck und Zürich über Nacht die Metropole im deutschen Norden– natürlich bequem und entspannt direkt das Zentrum beim Hamburger Hauptbahnhof.</p>
            <h2>Sehenswürdigkeiten</h2>
          <iframe width="600" height="450" frameborder="0" style={{border:'0',width: '100%'}} src="https://www.google.com/maps/embed/v1/view?zoom=17&amp;center=53.5505,9.9947&amp;key=AIzaSyANdaOz9h2HCgWlIs4hh402g0DeAi3i5KE" allow-fullscreen="true"></iframe>
          <p>Auf dem Weg in die Stadt hinein stattet man dem „Michel“ zur besseren Orientierung einen Besuch ab. Von der luftigen Plattform des barocken Kirchturms aus bietet sich in 106 Metern Höhe ein toller Überblick über die Stadt. Gegensätze ziehen sich bekanntlich an und so kann man sich von der Barockkirche gleich in eines der multikulturellen Szeneviertel Hamburgs aufmachen, um einen Kaffee – oder ein in Hamburg-Altona gebrautes Astra – zu trinken: das Karoviertel, die „Schanze“ und Eimsbüttel warten.</p> 
          <p>Für den Rückweg zum Hauptbahnhof sollte man dann noch etwas Energie und Lust auf Kunst übrig haben, denn praktischerweise liegt der Bahnhof inmitten des Hamburger Museumsviertels. Schlafen lässt es sich dann ja schließlich herrlich im Nightjet.</p>

        </div>
      );
    }
  }

    render() {
        return (

            <div id="scrollbar" class="uk-margin-medium-top">
                <div class="uk-container" style={{ minHeight: '300px' }}>
                <h1>Die große weite Welt und Seeluft schnuppern: Freie und Hansestadt Hamburg.</h1>
                </div>
                <div style={{ background: '#084358', paddingBottom: '20px', paddingTop: '30px' }}>
                    <div class="uk-container">
                        {this.renderPage()}
                    </div>
                </div>
            </div>


        );
    }
}   

