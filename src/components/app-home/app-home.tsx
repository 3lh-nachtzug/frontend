import { Component, Element } from '@stencil/core';

@Component({
    tag: 'app-home',
    styleUrl: 'app-home.css',
    shadow: false
})
export class AppHome {

  @Element() element: HTMLElement;

  handleFromToSubmit(e: CustomEvent) {
    this.element.querySelector('tn-from-to-result').refresh({
      evaFrom: e.detail.evaFrom,
      evaTo: e.detail.evaTo,
      depDate: e.detail.depDate
    });
  }
  componentWillLoad(){
    console.log("componentWillLoad")
    document.body.classList.remove("hamburg")
    document.body.classList.add("home")
  
    window.scrollTo(0,0);
  }
  render() {
      return (
        <div id="scrollbar" class="uk-margin-medium-top">
          <div class="uk-container" style={{ minHeight: '300px' }}>
            <tn-from-to onFromToSubmitted={(e: CustomEvent) => this.handleFromToSubmit(e)}></tn-from-to>
            <img src="/assets/images/scroll-arrow.svg" alt="" class="scroll-arrow" />
            <tn-from-to-result></tn-from-to-result>
          </div>

          <div style={{ background: '#084358', paddingBottom: '20px', paddingTop: '30px' }}>
            <div class="uk-container">
            <h2 style={{marginTop: '30px',marginBottom: '30px'}}>Trainite-Vorteile für dich</h2>
              <div class="uk-child-width-expand@s uk-text-center" uk-grid>
                <div class="icon-grid">
                  <img src="/assets/images/einfach.svg" alt="Einfach" />
                  <h3>Einfach</h3>
                  <p>Mit dem Zug durch die Nacht sparst du dir einen Reisetag und kommst direkt im Zentrum an.</p>
                </div>

                <div class="icon-grid"><img src="/assets/images/nachhaltigkeit.svg" alt="Nachhaltig" />
                  <h3>Nachhaltig</h3>
                  <p>Halte deinen ökologischen Fussabdruck klein - fahre mit dem Nachtzug!</p>
                </div>
  
                <div class="icon-grid"><img src="/assets/images/entspannung.svg" alt="Entspannt" />
                  <h3>Entspannt</h3>
                  <p>Während der ganzen Fahrt durch die Nacht lässt es sich im Schlaf-, Liege- oder Sitzwagen gut träumen.</p>
                </div>
              </div>

              <h2 style={{ textAlign: 'center', color: 'white', padding: '20px', fontSize: '40px' }}>Beliebteste Destinationen</h2>
              
              <div class="uk-child-width-1-3@m" uk-grid uk-height-match="target: .uk-card-body">
                  <div>
                      <div class="uk-card uk-card-default"  >
                          <div class="uk-card-media-top">
                              <img src="/assets/images/Hamburg-492694916-silverjohn-iStock-Thinkstock.jpg" alt="Hamburg" />
                          </div>
                          <div class="uk-card-body">
                              <h3 class="uk-card-title">Hamburg</h3>
                              <p>Die große weite Welt und Seeluft schnuppern: Freie und Hansestadt Hamburg.</p>
                          </div>
                          <div class="uk-card-footer">
                          <stencil-route-link url='/destination/hamburg'>Jetzt entdecken <span uk-icon="icon:  chevron-right; ratio: 2"></span></stencil-route-link>
                             
                          </div>
                      </div>
                  </div>
                  <div>
                      <div class="uk-card uk-card-default">
                          <div class="uk-card-media-top">
                              <img src="/assets/images/Wien-843292856-AND-ONE-iStock-Thinkstock.jpg" alt="" />
                          </div>
                          <div class="uk-card-body">
                              <h3 class="uk-card-title">Wien</h3>
                              <p>Zwischen gestern und heute: Ein Besuch in Wien.</p>
                          </div>
                          <div class="uk-card-footer">
                              <a href="#" class="uk-button uk-button-text">Jetzt entdecken <span uk-icon="icon:  chevron-right; ratio: 2"></span></a>
                          </div>
                      </div>
                  </div>
                  <div>
                      <div class="uk-card uk-card-default">
                          <div class="uk-card-media-top">
                              <img src="/assets/images/Zuerich-615269760-RossHelen-iStock-Thinkstock.jpg" alt="" />
                          </div>
                          <div class="uk-card-body">
                              <h3 class="uk-card-title">Zürich</h3>
                              <p>Kleine Stadt ganz groß: Zürich entdecken.</p>
                          </div>
                          <div class="uk-card-footer">
                              <a href="#" class="uk-button uk-button-text">Jetzt entdecken <span uk-icon="icon:  chevron-right; ratio: 2"></span></a>
                          </div>
                      </div>
                  </div>
              </div>

              <div class="uk-child-width-1-3@m" uk-grid uk-height-match="target: .uk-card-body">
                  <div>
                      <div class="uk-card uk-card-default">
                          <div class="uk-card-media-top">
                              <img src="/assets/images/Budapest-526554136-pisittar-iStock-Thinkstock.jpg" alt="" />
                          </div>
                          <div class="uk-card-body">
                              <h3 class="uk-card-title">Budapest</h3>
                              <p>Im Herzen Europas: Budapest entdecken.</p>
                          </div>
                          <div class="uk-card-footer">
                              <a href="#" class="uk-button uk-button-text">Jetzt entdecken <span uk-icon="icon:  chevron-right; ratio: 2"></span></a>
                          </div>
                      </div>
                  </div>
                  <div>
                      <div class="uk-card uk-card-default">
                          <div class="uk-card-media-top">
                              <img src="/assets/images/Mailand-528059198-ventdusud-istock-thinkstock.jpg" alt="" />
                          </div>
                          <div class="uk-card-body">
                              <h3 class="uk-card-title">Mailand</h3>
                              <p>Italienische Eleganz und ein Espresso in der Hand</p>
                          </div>
                          <div class="uk-card-footer">
                              <a href="#" class="uk-button uk-button-text">Jetzt entdecken <span uk-icon="icon:  chevron-right; ratio: 2"></span></a>
                          </div>
                      </div>
                  </div>
                  <div>
                      <div class="uk-card uk-card-default">
                          <div class="uk-card-media-top">
                              <img src="/assets/images/Prag-462479669-RudyBalasko-iStock-Thinkstock.jpg" alt="" />
                          </div>
                          <div class="uk-card-body">
                              <h3 class="uk-card-title">Prag</h3>
                              <p>Eine Stadt voller Historie: Prag entdecken.</p>
                          </div>
                          <div class="uk-card-footer">
                              <a href="#" class="uk-button uk-button-text">Jetzt entdecken <span uk-icon="icon:  chevron-right; ratio: 2"></span></a>
                          </div>
                      </div>
                  </div>
              </div>
            </div>
          </div>
        </div>
      );
  }
}   
